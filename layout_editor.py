import matplotlib.pyplot as plt
import pickle


class LayoutEditor():
    
    def __init__(self):
        _, self.ax = plt.subplots(1,1)

    def load_store(self, store):
        self.store = store
        self.plot()

    def plot(self):
        self.ax.cla()
        nodes = self.store.get_nodes()
        visited = set()
        for n in nodes:
            visited.add(n._id)
            self.ax.text(n.x, n.y, str(n._id))
            for nn in n.neighbs:
                if nn._id not in visited:
                    self.ax.plot([n.x, nn.x], [n.y, nn.y], '--')

    def move_node(self, node_id, new_x, new_y):
        found = None
        nodes = self.store.get_nodes()
        for n in nodes:
            if n._id == node_id:
                found = n
                break
        if not found:
            print('Node {0} not found'.format(node_id))
            return
        self.store.remove_node(found)
        found.move(new_x, new_y)
        self.store.add(found)
        print('Node moved to {0} {1}'.format(new_x, new_y))
        self.plot()

    def save_layout(self, area_width, net_size, radio_range, pickle_file):
        """
        Pickles the layout as a dictionary with the following keys:
            'nodes': list of (x,y) coordinates;
            'area_width': width of the layout area;
            'init_radio_range': initial radio range; this will correspond to
                                the highest PRR;
            'net_size': number of nodes
        """
        layout_dict = {
                'nodes': [(n.x, n.y) for n in self.store.get_nodes()],
                'area_width': area_width,
                'net_size': net_size,
                'init_radio_range': radio_range
                }
        pickle.dump(layout_dict, open(pickle_file, 'wb'))
