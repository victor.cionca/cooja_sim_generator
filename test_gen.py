"""
This generates a simulation file for Cooja.
It has a boilerplate header.

What can be controlled:
    - the radio model (select b/w UDGM and UDGM constant loss)
    - the TX and RX success rate
    - the TX power
    - the number and position of the nodes
"""

# ===============================================================
#HEADER
#
#In the header we control the random seed.
#The value can be "generated" or a number
header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<simconf>\n\
        <project EXPORT=\"discard\">[CONTIKI_DIR]/tools/cooja/apps/serial_socket</project>\n\
        <project EXPORT=\"discard\">[CONTIKI_DIR]/tools/cooja/apps/mrm</project>\n\
        <project EXPORT=\"discard\">[CONTIKI_DIR]/tools/cooja/apps/mspsim</project>\n\
        <project EXPORT=\"discard\">[CONTIKI_DIR]/tools/cooja/apps/avrora</project>\n\
        <project EXPORT=\"discard\">[CONTIKI_DIR]/tools/cooja/apps/mobility</project>\n\
        <simulation>\n<title>Data collection network using IPv6 and RPL</title>\n<delaytime>0</delaytime>\n<randomseed>%s</randomseed>\n<motedelay_us>5000000</motedelay_us><events>\n<logoutput>40000</logoutput>\n</events>\n"

#===============================================================
#RADIO MODEL
#
#The radio_model header has three parameters:
#- radio range and interference range
#- the type of the radio model.
#
#Right now you can choose b/w udgm and udgm with constant loss.
#
# ----------- IMPORTANT - the radio model has been moved to
# ----------- stand-alone classes
#===============================================================


#===============================================================
#MOTE APPLICATION
#
#Desription of a mote application.
#Parameters of string (s.format() is used)
#%mote_type identifier
#%path to file
#%filename (without extension)
#%firmware type (e.g sky, cooja)
mote_desc = " <motetype>\n\
        {mote_class}\n\
        <identifier>{mote_type}</identifier>\n\
        <description>Sky Mote Type #sky1</description>\n\
        <firmware EXPORT=\"copy\">{path}{filename}.{firmware}</firmware>\n\
        <moteinterface>org.contikios.cooja.interfaces.Position</moteinterface>\n<moteinterface>org.contikios.cooja.interfaces.RimeAddress</moteinterface>\n<moteinterface>org.contikios.cooja.interfaces.IPAddress</moteinterface>\n<moteinterface>org.contikios.cooja.interfaces.Mote2MoteRelations</moteinterface>\n<moteinterface>org.contikios.cooja.interfaces.MoteAttributes</moteinterface>\n\
        <moteinterface>org.contikios.cooja.mspmote.interfaces.Msp802154Radio</moteinterface>\n\
        <moteinterface>org.contikios.cooja.mspmote.interfaces.MspClock</moteinterface>\n\
        <moteinterface>org.contikios.cooja.mspmote.interfaces.MspSerial</moteinterface>\n"
#        <moteinterface>org.contikios.cooja.mspmote.interfaces.MspMoteID</moteinterface>\n\
#        <moteinterface>org.contikios.cooja.mspmote.interfaces.MspDebugOutput</moteinterface>\n\
#        </motetype>\n"

#===============================================================
#MOTE TEMPLATE
#
#The mote template is used when adding nodes to the simulation.
#It has three parameters, in this exact order:
#    x position (int), y position (int),
#    mote interface class (string)
#    mote id  (int), mote type ("sky1" for base station, "sky2" for regular)
mote_template = "<mote>\n<breakpoints />\n<interface_config>\norg.contikios.cooja.interfaces.Position\n<x>%d</x>\n<y>%d</y>\n<z>0.0</z>\n</interface_config>\n<interface_config>\n%s\n<id>%s</id>\n</interface_config>\n<motetype_identifier>%s</motetype_identifier>\n</mote>"

#===============================================================
#PLUGINS

#It is possible to log the packets into pcap format.
#The parameter is the path to the file where the packets will be logged.
packet_logger = "<plugin>\norg.contikios.cooja.plugins.RadioLoggerHeadless\n<plugin_config>\n<pcap_file>%s</pcap_file>\n</plugin_config></plugin>\n"
debug_logger = "<plugin>\norg.contikios.cooja.plugins.LogListenerHeadless\n<plugin_config>\n<append>%s</append></plugin_config></plugin>\n"

#Serial socket server.
#Allows simulated communication over the serial line between the mote and a client application.
#Parameters: (mote ID, socket port)
serial_server= "<plugin>\norg.contikios.cooja.serialsocket.SerialSocketServer\n<mote_arg>%d</mote_arg>\n<plugin_config>\n<port>%d</port>\n<bound>true</bound>\n</plugin_config>\n</plugin>\n"

#RealSim control plugin
#For more info, see https://github.com/cmorty/realsim/tree/master/cooja-plugin
realsim= "<plugin>de.fau.cooja.plugins.realsim.RealSimFile\n\
    <plugin_config>\n\
      <Filename>%s</Filename>\n\
      <Load>true</Load>\n\
    </plugin_config>\n\
  </plugin>"

#The simulation is controlled by a Javascript script.
#
#A simple script can be used which takes the experiment length
# as a parameter and stops when that time expires.
# The length of the experiment (in ms) is the parameter.
script_runner = "<plugin>\norg.contikios.cooja.plugins.ScriptRunner\n<plugin_config><script>\n\
TIMEOUT(%d);\n\
while (true) {\n\
YIELD();\n\
}\n\
</script><active>true</active></plugin_config></plugin>"

#A more complex script can be used which outputs the 
# logs to a file and catches an exception when there
# are no more messages.
script_runner2 = "<plugin>\norg.contikios.cooja.plugins.ScriptRunner\n<plugin_config><script>\n\
 //import Java Package to JavaScript\n\
 importPackage(java.io);\n\
\n\
 // Use JavaScript object as an associative array\n\
 outputs = new FileWriter(%s);\n\
\n\
 while (true) {\n\
    //Write to file.\n\
    outputs.write(time + \" \" + msg + \"\\n\");\n\
\n\
    try{\n\
        //This is the tricky part. The Script is terminated using\n\
        // an exception. This needs to be caught.\n\
        YIELD();\n\
    } catch (e) {\n\
        //Close files.\n\
        outputs.close();\n\
        //Rethrow exception again, to end the script.\n\
        throw('test script killed');\n\
    }\n\
 }\n\
</script><active>true</active></plugin_config></plugin>"

footer = "</simconf>"

mote_classes = {'sky':'org.contikios.cooja.mspmote.SkyMoteType',\
                'cooja':'org.contikios.cooja.contikimote.ContikiMoteType'}

class SimScriptBuilder:
    def __init__(self, filename, exp_length, radio_model, **kwargs):
        """Generates a Cooja CSC simulation configuration file. There are many 
        configuration options possible.

        Parameters:
        filename        -- Path to CSC file that is being generated.
        exp_length      -- Length of time (in ms) that simulation should run for.
        radio_model     -- A radio model object, such as UDGM or DGRM
        
        Keyworded arguments:
        enable_serial   -- Default True. Will create serial server sockets for each
                           added mote.
        base_serial     -- The ports are created sequentially starting
                           at base_serial (port = base_serial + mote id).
        """
        self.filename = filename
        self.radio_model = radio_model
        self.header = header % "generated"
        self.mote_desc_list = ""
        self.mote_list = ""
        self.exp_length = exp_length
        self.plugins = ""
        self.num_motes = 0
        # kwargs
        print("Testgen", kwargs)
        self.enable_serial = kwargs['enable_serial'] if 'enable_serial' in kwargs else False
        self.base_serial = kwargs['base_serial'] if 'base_serial' in kwargs else None

    def set_random_seed(self, random_seed):
        """Specifies a fixed random seed to be used by this simulation.

        This allows a certain configuration to be precisely repeated.
        """
        self.header = header % str(random_seed)

    def add_mote_app(self, mote_type, app_path, fw='sky'):
        """Add a mote application to the simulation.

        Parameters:
        mote_type       -- String label associated to this application (e.g 'sky1').
        app_path        -- Path to the application firmware (one that ends in e.g '.sky').
        fw              -- The type of mote that will run this application. At the moment,
                           only the sky mote and Contiki mote are supported.
        """
        fields = app_path.split('/')
        filename = fields[-1].split('.')[0]
        path = '/'.join(fields[:-1])
        if len(path) > 0:
            path += '/'
        else:
            path = '[CONFIG_DIR]/'
        self.mote_desc_list += mote_desc.format(mote_class=mote_classes[fw], mote_type=mote_type, path=path, filename=filename, firmware=fw)
        if fw == 'cooja':
            self.mote_desc_list += "<moteinterface>org.contikios.cooja.contikimote.interfaces.ContikiMoteID</moteinterface>\n"
        else:
            self.mote_desc_list += "<moteinterface>org.contikios.cooja.mspmote.interfaces.MspMoteID</moteinterface>\n"
        self.mote_desc_list += "</motetype>\n"

    
    def add_mote(self, mote_x, mote_y, mote_id, mote_type):
        """Adds a mote to the simulation, based on position, id and type.
        If this configuration supports serial sockets, a serial server socket
        will be generated for this mote, with address (localhost, base_serial+mote_id).

        Parameters:
        mote_x      -- The x position on the simulated network map.
        mote_y      -- The y position on the simulated network map.
        mote_id     -- The numeric id associated with the mote, used to generate
                       the link local address.
        mote_type   -- String label that indicates what application will run on
                       this simulated mote.
        """
        #print "Adding %d @ (%d,%d), type=%s" % (mote_id, mote_x, mote_y, mote_type)
        if mote_type.startswith('cooja'):
            mote_id_iface = "org.contikios.cooja.contikimote.interfaces.ContikiMoteID"
        else:
            mote_id_iface = "org.contikios.cooja.mspmote.interfaces.MspMoteID"
        self.mote_list += mote_template % (mote_x, mote_y, mote_id_iface, mote_id, mote_type)
        # if we support serial sockets, add a new plugin
        if self.enable_serial:
            self.plugins += serial_server % (self.num_motes, self.base_serial + self.num_motes)
        self.num_motes += 1


    def enable_logging(self, path):
        """Enables the PCAP packet sniffer plugin.

        Parameters:
        path    -- Path where to save the packet capture.
        """
        self.plugins += packet_logger % path

    def enable_debug(self, path):
        """Enables the logging of debug (printf) messages.

        Parameters:
        path    -- Path where to save the log file.
        """
        self.plugins += debug_logger % path

    def enable_realsim(self, path):
        """Enables the realsim plugin that allows runtime
        changes to the network (add/remove nodes and edges).
        Requires DGRM.

        Parameters:
        path    -- Path to the realsim control file.
        """
        self.plugins += realsim % path

    def generate(self):
        if not (self.radio_model):
            raise FormatException()
        if len(self.mote_desc_list) == 0 or len(self.mote_list) == 0:
            raise FormatException()
        f = open(self.filename, 'w')
        f.write(self.header)
        f.write(self.radio_model.output())
        f.write(self.mote_desc_list)
        f.write(self.mote_list)
        f.write("</simulation>")
        # write the plugins
        f.write(self.plugins)
        # write the script runner plugin
        f.write(script_runner % self.exp_length)
        f.write(footer)
        f.close()

