from cooja_control.network_builder import Node, NodeStore
import math

class GridLayout():
    """
    This class generates a 4x4 grid network of nodes separated
    by the same distance on all sides from their neighbours
    """

    def __init__(self, num_nodes, distance):
        self.num_nodes = num_nodes
        self.distance = distance
        self.nodes_per_row = int(math.ceil(math.sqrt(self.num_nodes)))
        self.net_area_side = (self.nodes_per_row - 1)*self.distance
        self.net_area_diag = math.sqrt(2)*self.net_area_side
        self.area_width = self.net_area_side
        self.nodes = None

    def get_net_area_diagonal(self):
        """Computes the diagonal of the area occuppied by the 
        network nodes. The area is a square
        """
        return self.net_area_diag

    def generate_net(self):
        """Generate the coordinates of the network nodes
        """
        if not self.nodes == None:
            return self.nodes

        self.nodes = []
        for i in range(self.num_nodes):
            _x = (i % self.nodes_per_row) * self.distance
            _y = int(i/self.nodes_per_row) * self.distance
            self.nodes.append((_x, _y))
        return self.nodes

    def net_store_from_net(self, comms_range):
        """Builds a network store (see network_builder) based on net
        """
        if not self.nodes:
            self.generate_net()

        store = NodeStore(int(self.net_area_diag), int(comms_range))
        for i,n in enumerate(self.nodes):
            newnode = Node(n[0], n[1], _id=i)
            neighbs = store.find_neighbours(newnode)
            for nb in neighbs:
                nb.neighbs.append(newnode)
                newnode.neighbs.append(nb)
            store.add(newnode)
        return store


class GridLayoutComplex():
    """This class generates grid networks with arbitraty number
    of neighbours. Actually only works for 4 and 6.

    Note: this shouldn't be used
    """
    def add_line(start_x, start_y, line_list):
        """Adds line to line_list, starting at start_x, start_y.

        The members of line_list are tuples (x, y).
        The data is not inserted if there's already a line there.
        """
        i = 0
        while i < len(line_list) and start_y > line_list[i][1]:
            i += 1
        if i == len(line_list):
            line_list.append((start_x, start_y))
            #print ">Add line: ", start_x, start_y, line_list
        elif start_y < line_list[i][1]:
            line_list.insert(i, (start_x, start_y))
            #print ">Add line: ", start_x, start_y, line_list

    def closest_int(real):
        """
        This returns the closest integer to the real.
        If the real is within [int-0.5, int+0.5] we 
        return int. Otherwise, int-1 or int+1.
        """
        tmp = int(abs(real)*100)
        quot = tmp/100
        remd = tmp % 100
        signum = real/abs(real) if real != 0 else 0
        if remd >= 50:
            return signum*(abs(quot)+1)
        else:
            return signum*(abs(quot))
        
    def grid_net_complex(net_size, dist, num_neighbs):
        """Lay out nodes in a grid. Returns list of nodes' positions as (x,y) tuples.
        Only 4 and 6 nodes work (regular tilings)

        This creates a network where the nodes are laid out on a grid,
        such that the number of neighbours is the same for each node,
        except for the nodes on the edges.

        The number of nodes does not need to be specified - nodes will 
        be added until they fill the network.
        """

        """
        The nodes are laid out at angles on a concentric circle centered
        on the node. The algorithm starts by placing a node in the 
        top-left corner (0,0). Grid lines will always have nodes spaced
        out evenly, so that's no problem.
        The algorithm must compute, for each start of line, the start of
        the next lines. It stores these in order of the y position.
        Depending on the number of neighbours desired, a node will generate
        one or more next lines. This means that those lines might also
        generate next lines, before the original lines have been finalised.
        """
        
        # node store
        store = []

        # angle per neighbour
        alpha = 2*math.pi/num_neighbs
        # angle where we start searching for next line
        start_alpha = 0
        if int(math.pi/alpha)*alpha == math.pi:
            start_alpha = math.pi
        else:
            start_alpha = (int(math.pi/alpha)+1)*alpha
        next_lines = [(0,0)]
        while len(next_lines) > 0:
            x, y = next_lines.pop(0)
            if y > net_size:
                # we stop when we reach the end of the area
                break
            x_pos = x
            #print "New line starting at ", x, y
            while x_pos <= net_size:
                # add node to store
                store.append((x_pos, y))
                x_pos += dist
            # compute now the next possible lines
            alpha_tmp = start_alpha
            while alpha_tmp < 2*math.pi-0.00001:
                next_x = x + math.cos(alpha_tmp)*dist
                next_y = y + math.sin(alpha_tmp - math.pi)*dist
                next_x = int(closest_int(next_x))
                next_y = int(closest_int(next_y))
                #print alpha_tmp, next_x, next_y
                if next_x >= 0:
                    # good position, let's insert it in the list
                    add_line(next_x, next_y, next_lines)
                alpha_tmp += alpha
        return store

#####################################################
# The following functions have no business being here.
# This should just create the layout as an array of 
# (x,y) tuples.
#####################################################
#from test_gen import SimScriptBuilder, udgm_ct_loss_radio
def generate_script(script_file, net_size, neighbs, radio_range):
    """Generate a network then include it in a script
    """
    store = grid_net(net_size, radio_range, neighbs)
    # now it's time to build the simulation script
    script_builder = SimScriptBuilder(script_file, udgm_ct_loss_radio, radio_range+10)
    script_builder.write_headers()
    script_builder.add_mote(0, 0, 0, 'sky1')
    for (i,n) in enumerate(store[1:]):
        script_builder.add_mote(n[0], n[1], i+1, 'sky2')
    script_builder.done_adding_motes()
    script_builder.enable_logging('sim_log.pcap')
    script_builder.set_script('script1_ctrl.js')
    script_builder.finalise()
    
# This on does the same as the above only doesn't generate the network
def generate_script_with_net(base_path, radio_range, net, exp_length, mote_types, seed=None):
    """Generate a script file based on an already laid out network

    Keyword arguments
    base_path  -- base path of csc and pcap files
    radio_range-- radio range
    net        -- the list of nodes with their coordinates
    exp_length -- length of experiment in seconds
    mote_types -- list of dict of type {type, path, number} describing mote types.
                  The motes are instantiated in the given order.
    seed       -- Specify the random seed of the simulation. Default is generated automatically
    """
    # now it's time to build the simulation script
    script_builder = SimScriptBuilder(base_path+'.csc', udgm_ct_loss_radio, radio_range)
    if seed:
        script_builder.set_random_seed(seed)
    for mt in mote_types:
        # if firmware is not defined...
        fw = None
        if 'firmware' not in mt:
            fw = 'sky'
        script_builder.add_mote_app(mt['type'], mt['path'], fw)
    # add the motes mapped on the network
    mt = 0
    mt_motes = 0
    #print mote_types
    for (i,n) in enumerate(net):
        if mote_types[mt]['number'] <= mt_motes:
            mt += 1
            mt_motes = 0
        if mt >= len(mote_types):
            print(mt)
            print("[ERROR] Provided less motes than network size")
            break
        script_builder.add_mote(n[0], n[1], i, mote_types[mt]['type'])
        mt_motes += 1
    script_builder.enable_logging(base_path+'.pcap')
    script_builder.enable_debug(base_path+'.log')
    script_builder.set_exp_length(exp_length*1000)
    script_builder.generate()
    

def grids_increasing_num_neighbs(net_size, tiles, tile_dist):
    """Generates networks with increasing number of neighbours

    Start with a @tiles grid at @tile_dist distance.
    Initially radio range is @tile_dist.
    It is then repeatedly multiplied by sqrt(2) to increase the number of neighbours.

    We generate the network and then just increase the comms range
    """
    
    # first generate the network
    net = grid_net(net_size, tile_dist, tiles)

    neighb_count = dict()
    radio_range = tile_dist
    # maximum radio_range = half of net_size
    while radio_range < net_size/2:
        store = net_store_from_net(net, net_size, radio_range)
        neighb_count[radio_range] = store.get_max_neighbs()
        radio_range = radio_range * math.sqrt(2)
    print(neighb_count)

if __name__ == '__main__':
    grid_net(500, 100, 6, 'grid_net.csc')

