"""
This builds a clustered network that simulates a WSN deployment.

Nodes are clustered around 'interest areas'.

Interest areas will have a specified average number of nodes (cluster size).

The overall deployment area is split into square cells of width=2*max_TX.
Interest areas (cells) are allocated in a random-walk pattern.

This layout uses a fixed area width, as compared to the generic clustered
layout where the area width is calculated based on the number of nodes.
"""

import cooja_control.net_link_quality_analyser as LQA
from math import sqrt, pi, ceil
from random import random, randint, choice, gauss
from cooja_control.network_builder import NodeStore, Node
from copy import deepcopy

def distance(n1, n2):
    """If n1 and n2 are (x,y) tuples, compute euclidian distance bw them"""
    x1,y1 = n1
    x2,y2 = n2
    return sqrt((x1-x2)**2 + (y1-y2)**2)

class RandomWalkSpaceManager():
    """
    Represent a square area made up of cells.
    Cells are allocated randomly.
    Finding a new empty cell is O(1).
    """

    def __init__(self, area_width, cell_width):
        self.area_width = area_width
        self.cell_width = cell_width
        self.row_size = int(area_width/cell_width)
        self.num_cells = (self.row_size)**2
        self.empty_cells = list(range(int(self.num_cells)))
        self.taken_cells = list()
        self.crt_cell = 0

    def random_walk(self, cell_id):
        """Random walk in manhattan model"""
        options = []
        if cell_id > self.row_size:
            if cell_id - self.row_size not in self.taken_cells:
                options.append(cell_id - self.row_size)
        if cell_id%self.row_size > 0:
            if cell_id - 1 not in self.taken_cells:
                options.append(cell_id - 1)
        if (cell_id+1)%self.row_size != 0:
            if cell_id + 1 not in self.taken_cells:
                options.append(cell_id+1)
        if cell_id < self.num_cells - self.row_size:
            if cell_id + self.row_size not in self.taken_cells:
                options.append(cell_id + self.row_size)
        if len(options) > 0:
            return choice(options)
        else:
            return None

    def allocate_space(self):
        """Allocate a new cell from the area.

        Use a random walk to select the next cell.
        Select a random allocated cell and use random walk from it
        to select the next one.

        Returns None if no more empty cells are available.
        """
        if len(self.empty_cells) == 0:
            return None

        next_cell = self.crt_cell
        self.crt_cell += 1
        #next_cell = None
        #while next_cell is None:
        #    if len(self.taken_cells) == 0:
        #        crt = choice(self.empty_cells)
        #    else:
        #        crt = choice(self.taken_cells)
        #    next_cell = self.random_walk(crt)
        self.empty_cells.remove(next_cell)
        self.taken_cells.append(next_cell)
        cell_row = int(next_cell / self.row_size)
        cell_column = next_cell % self.row_size
        cell_x = cell_column * self.cell_width
        cell_y = cell_row * self.cell_width
        cell_centre = (cell_x + int(self.cell_width/2), cell_y + int(self.cell_width/2))
        #print(cell_centre, ',')
        return cell_centre

    def free_space(self, cluster_centre):
        """Free the cell centred on the cluster centre coordinates.

        De-allocation is only performed for cell centres, not for randomly
        allocated points.
        """
        # Determine if and in which cell this lies. The x and y must be on 
        # cell borders.
        cell_x = cluster_centre[0] - self.cell_width/2
        cell_y = cluster_centre[1] - self.cell_width/2
        if cell_x % self.cell_width == 0 and cell_y % self.cell_width == 0:
            cell_id = cell_y/self.cell_width*self.row_size\
                      + cell_x/self.cell_width
            if cell_id in self.taken_cells:
                self.taken_cells.remove(cell_id)
                self.empty_cells.append(cell_id)
            else:
                raise Exception("Free space centred at"+str(cluster_centre)+
                                "not found in taken cells"+
                                str(self.taken_cells)+str(self.empty_cells))
        return

class CompPeer():
    def __init__(self, comp, dist, n, m):
        """
        The closest distance to a peer component.

        Parameters:
        comp    -- the component
        dist    -- distance to peer
        n,m     -- nodes where the distance is shortest
        """
        self.comp = comp
        self.dist = dist
        self.n = n
        self.m = m

    def __repr__(self):
        return str(self.comp.centre)

import networkx as nx
class NwkComponent():
    def __init__(self, nodes, min_link_prr):
        """
        Params:
        nodes -- list of Nodes, with x, y, and _id
        """
        self.nodes = nodes
        self.centre = (sum(n.x for n in nodes)/float(len(nodes)),
                       sum(n.y for n in nodes)/float(len(nodes)))
        self.max_link_distance = LQA.get_max_dist_for_prr(min_link_prr)
        # List to hold other components, sorted in distance from me
        self.peers = []

    def merge(self, other):
        """
        Merge this component with another one, by connecting the centres.

        Returns: list of connecting nodes as (x,y) coordinates.
        """
        # Average distance between nodes - determines number of connecting nodes
        avg_inter = (random()+1)*(self.max_link_distance/2)
        # Distance between centres
        distance = sqrt((other.centre[0]-self.centre[0])**2 +
                        (other.centre[1]-self.centre[1])**2)
        # Compute angle of line uniting centres
        sinalpha = (other.centre[1] - self.centre[1])/distance
        cosalpha = (other.centre[0] - self.centre[0])/distance
        # Add the connecting nodes
        nodes = []
        print(distance, avg_inter)
        while distance > avg_inter:
            d = gauss(avg_inter, (self.max_link_distance-avg_inter)/4)
            distance -= d
            x = other.centre[0] - distance*cosalpha
            y = other.centre[1] - distance*sinalpha
            nodes.append((x,y))
            print(distance, avg_inter)
        return nodes

    def compute_peers(self, components):
        """
        Each peer is a component and for each component we determine
        the minimum distance between us and them.
        """
        def _dist(n1, n2):
            return sqrt((n1.x-n2.x)**2 + (n1.y-n2.y)**2)
        self.peers = []
        for c in components:
            if c == self: continue
            # For each of our nodes, find the min distance to the other comp
            min_dist = None
            for n in self.nodes:
                for m in c.nodes:
                    d = _dist(n,m)
                    if min_dist is None or d < min_dist.dist:
                        min_dist = CompPeer(c, d, n, m)
            self.peers.append(min_dist)
        self.peers.sort(key=lambda x:x.dist)

    def __eq__(self, other):
        """Override equality check, compare list of nodes"""
        return self.nodes == other.nodes

class ConnectivityManager():
    def __init__(self, nodestore, min_link_prr):
        """
        Params:
        nodestore   -- NodeStore containing nodes and neighbour relations
        """
        self.network = nx.Graph()
        # First add all the nodes
        self.network.add_nodes_from(nodestore.get_nodes())
        for n in nodestore.get_nodes():
            for nb in nodestore.find_neighbours(n):
                self.network.add_edge(n,nb)
        self.components = [NwkComponent(c, min_link_prr) for c in
            sorted(nx.connected_components(self.network), key=len, reverse=True)]
        self.max_link_distance = LQA.get_max_dist_for_prr(min_link_prr)

    def merge(self, n1, n2):
        """
        Connect the two nodes using a line of nodes roughly evenly spaced.

        Returns: list of connecting nodes as (x,y) coordinates.
        """
        #print "\t Merging", (n1.x, n1.y), (n2.x, n2.y)
        # Average distance between nodes - determines number of connecting nodes
        avg_inter = (random()+1)*(self.max_link_distance/2)
        # Distance between nodes
        distance = sqrt((n2.x-n1.x)**2 +
                        (n2.y-n1.y)**2)
        # Compute angle of line uniting nodes
        sinalpha = (n2.y - n1.y)/distance
        cosalpha = (n2.x - n1.x)/distance
        # Add the connecting nodes
        nodes = []
        while distance > avg_inter:
            d = gauss(avg_inter, (self.max_link_distance-avg_inter)/4)
            distance -= d
            x = n2.x - distance*cosalpha
            y = n2.y - distance*sinalpha
            nodes.append((x,y))
        return nodes

    def join_components(self):
        """Join all the components into one.
        Returns - joining nodes
        """
        # Nodes for joining the components
        nodes = []
        # List to hold components of the whole
        whole = []
        component_list = list(self.components)
        largest_comp = component_list[0]
        component_list = component_list[1:]
        largest_comp.compute_peers(component_list) # Exclude noone from the peers
        whole.append(largest_comp)
        while len(component_list) > 1:
            # Find the closest component to the whole
            closest = None # (comp_in_whole, CompPeer)
            for c in whole:
                if closest is None or c.peers[0].dist < closest[1].dist:
                    closest = (c, c.peers[0])
            # Remove the closest peer from the whole's peerage
            for c in whole:
                found_at = None
                for idx,p in enumerate(c.peers):
                    if p.comp == closest[1].comp:
                        found_at = idx
                        break
                del c.peers[found_at]
            #print "Merging component at:", closest[0].centre, "with", closest[1].comp.centre
            # Merge the two points from the peer
            newnodes = self.merge(closest[1].n, closest[1].m)
            #print "New nodes:", newnodes
            nodes.extend(newnodes)
            component_list.remove(closest[1].comp)
            # Expand the closest's peers
            closest[1].comp.compute_peers(component_list) # Exclude components already in the whole
            whole.append(closest[1].comp)
        # Add the last remaining component
        c = component_list[0]
        c.compute_peers(self.components)
        nodes.extend(self.merge(c.peers[0].n, c.peers[0].m))
        return nodes

class FrequencySortedDegrees():
    """
    A class that stores nodes in sorted order of their degree's
    weight in the node degree distribution, which is the count*degree.
    As nodes are removed from the list the list stays ordered.
    """

    def __init__(self, source):
        """
        Params:
        source      -- original list of (node, degree) tuples
        """
        self.unit_weight = 1/len(source)
        self.degree_list = dict()
        self.degree_weights = dict()
        for n, deg in source:
            if deg not in self.degree_list:
                self.degree_list[deg] = []
                self.degree_weights[deg] = 0
            self.degree_list[deg].append((n, deg))
            self.degree_weights[deg] += self.unit_weight*deg
        self.degree_weights = sorted(self.degree_weights.items(),
                                    key=lambda x:x[1],
                                    reverse=True)

    def get_highest_weight_node(self):
        """
        Returns one of the nodes with highest weight.
        Removes the node from the list and updates the weights and weight
        order.
        """
        max_weight_degree = self.degree_weights[0][0]
        node_info = self.degree_list[max_weight_degree][0]
        del self.degree_list[max_weight_degree][0]
        new_weight = self.degree_weights[0][1]\
                    - self.unit_weight*max_weight_degree
        # Update the node degree weight order
        del self.degree_weights[0]
        index = 0
        while index < len(self.degree_weights) and new_weight < self.degree_weights[index][1]:
            index += 1
        self.degree_weights.insert(index, (max_weight_degree, new_weight))
        return node_info

    def get_degree_weights(self):
        return self.degree_weights

    def __repr__(self):
        return str(self.degree_weights)

from time import sleep
class ClusteredLayout():

    def __init__(self, num_nodes, area_width, subarea_width, target_density,
                min_link_prr=0.5):
        """
        Build a clustered graph of num_nodes in the deployment of area area_width.
        Cluster area width is subarea_width.
        target_density is the target average node degree of the graph
        ====================
        Starts by evenly distributing the nodes in the available clusters.
        Nodes are positioned at random within the clusters.
        In step 2 the clusters are connected, if they are disconnected.
        In step 3 the average node degree is calculated and adjusted towards the
        target density by moving nodes from high degree clusters into low
        degree clusters.
        """
        self.num_clusters = ceil(area_width/subarea_width)**2
        self.subarea_width = subarea_width
        self.min_link_prr = min_link_prr
        self.area_width = area_width
        self.num_nodes = num_nodes
        self.cluster_size_mean = num_nodes//self.num_clusters
        self.cluster_stddev = 2
        self.target_density = target_density
        self.nodes = []
        self.store = NodeStore(int(sqrt(2)*self.area_width),
                               LQA.get_max_dist_for_prr(min_link_prr))
        self.clusters = []
        self.node_degree = None
        # Minimum distance between nodes for PRR = 0.99
        self.min_distance = int(LQA.get_max_dist_for_prr(0.99))
        # -------------
        # For debugging purposes
        #self.orig_nodes = []
        #self.joining_nodes = []
        #self.extra_nodes = []

    def get_avg_node_degree(self):
        """Compute and return the average node degree (number of neighbours)"""
        if self.node_degree is None:
            node_dgrs = [len(self.store.find_neighbours(n)) for n in self.store.get_nodes()]
            self.node_degree = sum(node_dgrs)/float(len(node_dgrs))
        return self.node_degree

    def sort_freq(self, alist):
        """
        Sort a list of (node, node_degree) in decreasing order of the frequency
        of the node degree.
        Returns a list of lists, each containing all (node, node_degree) with the
        same node degree
        """
        val_lists = dict()
        for (node, node_degree) in alist:
            if node_degree not in val_lists:
                val_lists[node_degree] = []
            val_lists[node_degree].append((node, node_degree))
        lists = list(val_lists.values())
        lists.sort(key=lambda x: len(x), reverse=True)
        return lists

    def generate_net(self):
        """Generate the network layout.  """
        # Randomness in:
        # - cluster locations
        # - number of nodes per cluster: sample from a normal distribution.

        # Define the cluster width to be max TX range
        nwk_area = RandomWalkSpaceManager(self.area_width, self.subarea_width)

        max_nodes_per_cluster = self.cluster_size_mean + 3*self.cluster_stddev
        while (self.num_nodes - len(self.nodes)) > max_nodes_per_cluster:
            # Find a cluster centre that preferably does not overlap with existing
            cluster_centre = nwk_area.allocate_space()
            if cluster_centre in self.clusters:
                # This has already been allocated, continue
                continue
            if cluster_centre is None:
                break
            # Add nodes to the cluster
            cluster_size = gauss(self.cluster_size_mean, self.cluster_stddev)
            for i in range(int(cluster_size)):
                node_x = randint(cluster_centre[0] - int(self.subarea_width/2),
                                 cluster_centre[0] + int(self.subarea_width/2))
                node_y = randint(cluster_centre[1] - int(self.subarea_width/2),
                                 cluster_centre[1] + int(self.subarea_width/2))
                self.nodes.append((node_x, node_y))
            #sleep(1)

        # Add the nodes to the store
        for nid, n in enumerate(self.nodes):
            self.store.add(Node(n[0], n[1], nid))

        # --- For debugging
        # self.orig_nodes = list(self.nodes)
        # --- end


        nid = len(self.nodes)

        # Now check that the resulting graph is connected
        print('Checking connected')
        cm = ConnectivityManager(self.store, self.min_link_prr)
        more_nodes = []
        if len(cm.components) > 1:
            print("There are", len(cm.components), "components")
            more_nodes = cm.join_components()
        self.nodes.extend(more_nodes)
        for (x,y),_id in zip(more_nodes, range(nid,nid+len(more_nodes))):
            self.store.add(Node(x, y, _id))
        print("There are", len(cm.components), "components")

        inter_nodes = deepcopy(self.store.get_nodes())

        # --- For debugging
        # self.joining_nodes = list(more_nodes)
        # --- end

        # If there are nodes remaining, add them such that we approach the
        # target density:
        # * if we are under the target density add the nodes close to the
        #   high density areas
        # * if we are above the target density add them around sparse areas

        # What is the current average degree?
        node_degrees = [(n, len(n.neighbs)) for n in self.store.get_nodes()]
        avg_degree = sum(list(zip(*node_degrees))[1])/len(node_degrees)
        if avg_degree > self.target_density:
            dsec = sorted(node_degrees, key=lambda x: x[1])
        else:
            dsec = sorted(node_degrees, key=lambda x: x[1], reverse=True)

        print("Distributing", self.num_nodes - len(self.nodes))
        while (self.num_nodes - len(self.nodes)) > 0:
            destination = dsec[0][0]
            dest_x = destination.x + randint(0, self.min_distance) - \
                    2*randint(0, self.min_distance)
            dest_y = destination.y + randint(0, self.min_distance) - \
                    2*randint(0, self.min_distance)
            self.nodes.append((dest_x, dest_y))
            try:
                self.store.add(Node(dest_x, dest_y, nid))
            except IndexError:
                print (destination.x, destination.y, dest_x, dest_y)
                raise IndexError
            # Update the node degrees
            node_degrees = [(n, len(n.neighbs)) for n in self.store.get_nodes()]
            avg_degree = sum(list(zip(*node_degrees))[1])/len(node_degrees)
            if avg_degree > self.target_density:
                dsec = sorted(node_degrees, key=lambda x: x[1])
            else:
                dsec = sorted(node_degrees, key=lambda x: x[1], reverse=True)
            nid += 1

        # Shuffle the nodes around to meet the target distribution

        # The node degree has a certain distribution.
        # We want to change the mean of the distribution to the target value
        # For example, if the target is below the current degree we will
        # move some of the nodes with degree closest to the mean to lower degrees.
        # This will lower the mean.

        node_degrees = [(n, len(n.neighbs)) for n in self.store.get_nodes()]
        avg_degree = sum(list(zip(*node_degrees))[1])/len(node_degrees)

        print('Target density', self.target_density, 'Current', avg_degree)

        # 1. Get the histogram of the node degrees
        #dprime = FrequencySortedDegrees(node_degrees)

        # 2. Calculate the difference between the target and current degree.
        # 3. Set of nodes D' <- nodes sorted in decr order of frequency of
        #    the node degree
        if avg_degree > self.target_density:
            dprime = sorted(node_degrees, key=lambda x: x[1], reverse=True)
            # 4. nodes D'' <- nodes sorted in increasing order of the degree.
            dsec = sorted(node_degrees, key=lambda x: x[1])
        else:
            dprime = sorted(node_degrees, key=lambda x: x[1])
            # 5. nodes D'' <- nodes sorted in decr order of the node degree
            dsec = sorted(node_degrees, key=lambda x: x[1], reverse=True)
        # 6. Go through the nodes in D' and D'' simultaneously removing and
        #    resp. adding nodes, until we meet the target difference.
        moved = 0
        attempts = 0
        while (0.5 < abs(avg_degree - self.target_density)):
            delta_total = 0
            # Take the node with the highest frequency degree
            node_info = dprime[0]
            # Remove the node from the store
            self.store.remove_node(node_info[0])
            # Update the delta with the node degree that was removed
            if self.target_density < avg_degree:
                delta_total += node_info[1]
            else:
                delta_total -= node_info[1]
            # Move the node close to the node with the smallest/highest degree
            destination = dsec[0][0]
            dest_x = destination.x + randint(0, int(self.min_distance/2)) - \
                    2*randint(0, int(self.min_distance/2))
            dest_y = destination.y + randint(0, int(self.min_distance/2)) - \
                    2*randint(0, int(self.min_distance/2))
            node_info[0].move(dest_x, dest_y)
            self.store.add(node_info[0])
            # What is the node's degree at the new location?
            if self.target_density < avg_degree:
                delta_total -= len(node_info[0].neighbs)
            else:
                delta_total += len(node_info[0].neighbs)
            # If the delta total is negative then we stop because we aren't
            # improving the total anymore
            if delta_total < 0:
                if attempts == 1:
                    break
                attempts += 1
            else:
                attempts = 0
            # Update the node degree to account for the new one
            node_degrees = [(n, len(n.neighbs)) for n in self.store.get_nodes()]
            avg_degree = sum(list(zip(*node_degrees))[1])/len(node_degrees)
            if avg_degree > self.target_density:
                dprime = sorted(node_degrees, key=lambda x: x[1], reverse=True)
                dsec = sorted(node_degrees, key=lambda x: x[1])
            else:
                dprime = sorted(node_degrees, key=lambda x: x[1])
                dsec = sorted(node_degrees, key=lambda x: x[1], reverse=True)
            moved += 1
            #dprime = FrequencySortedDegrees(node_degrees)
        print('Moved {0} nodes'.format(moved))

        node_degrees = [(n, len(n.neighbs)) for n in self.store.get_nodes()]
        avg_degree = sum(list(zip(*node_degrees))[1])/len(node_degrees)
        print('Target density = {0}. Current density = {1}'.format(self.target_density, avg_degree))
        return self.nodes, self.store.get_nodes(), inter_nodes
