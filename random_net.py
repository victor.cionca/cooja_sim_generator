"""
This creates a network where the nodes are randomly
placed on the area. Therefore, there are no 
guarantees on the number of neighbours or connectivity.
"""
from cooja_control.network_builder import *
from random import randint
from math import sqrt

class RandomLayout():
    def __init__(self, area_width, area_height, num_nodes, radio_range):
        self.area_width = area_width
        self.area_height = area_height
        self.num_nodes = num_nodes
        self.radio_range = radio_range

    def generate_net(self):
        """
        Creates a network of @net_size, with @num_nodes 
        communicating over @radio_range

        Returns the list of nodes
        """
        net_size = self.area_width
        net_diag = int(sqrt(2)*net_size)
        store = NodeStore(net_diag, self.radio_range)
        for i in range(self.num_nodes):
             n = Node(randint(0,self.area_width), randint(0,net_size))
             neighbs = store.find_neighbours(n)
             for nb in neighbs:
                 nb.neighbs.append(n)
                 n.neighbs.append(nb)
             store.add(n)
        nodes = store.get_nodes()
        return [(n.x, n.y) for n in nodes]

if __name__ == '__main__':
    rand_layout = RandomLayout(50, 50, 50)
    print(rand_layout.generate_net())
