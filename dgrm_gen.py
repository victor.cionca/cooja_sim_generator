"""
This creates a DGRM (directed graph model) configuration
for a given network, to use in a Cooja simulation.
"""

from cooja_control.network_builder import Node, NodeStore
from math import sqrt
import cooja_control.net_link_quality_analyser as LQA

class DGRMEdge:
    def __init__(self, src, dest):
        self.src = src
        self.dest = dest    # DGRMDest
        self.config = "<edge>\n<source>%d</source>\n<dest>\n%s\n</dest>\n</edge>" % \
                      (src, dest)

    def __str__(self):
        return self.config

    def __repr__(self):
        return self.config

class DGRMDest:
    def __init__(self, node_id, rx_ratio, rssi, lqi):
        self.id = node_id
        self.prr = rx_ratio
        self.rssi = rssi
        self.lqi = lqi
        self.config = "\
                org.contikios.cooja.radiomediums.DGRMDestinationRadio\n\
                <radio>%d</radio>\n\
                <ratio>%f</ratio>\n\
                <signal>%f</signal>\n\
                <lqi>%d</lqi>\n\
                <delay>0</delay>" % (node_id, rx_ratio, rssi, lqi)

    def __repr__(self):
        return self.config

class DGRMConfig(object):
    def __init__(self, min_link_prr):
        self.min_link_prr = min_link_prr
        self.edges = []
        self.node_degree_distr = []

    def add_edge(self, src_id, dst_id, rx_ratio, rssi, lqi):
        dst = DGRMDest(dst_id, rx_ratio, rssi, lqi)
        self.edges.append(DGRMEdge(src_id, dst))

    def build_from_net(self, net, area_width):
        """Builds a DGRM model for the given network.
        Uses the Link Quality Analyser.

        Important: assumes that the node ids start at 0,
        and they correspond to the index in the list

        Parameters:
        net         -- list of (x,y) for nodes
        area_width  -- width of the area where the nodes are laid out
        """
        # build a node store from the net, to obtain neighbours for node

        max_range = LQA.get_max_dist_for_prr(self.min_link_prr)
        net_diag = int(sqrt(2)*area_width)
        store = NodeStore(net_diag, max_range)
        for index, (x,y) in enumerate(net):
            n = Node(x, y, index)
            store.add(n)
        # initialise and build the DGRM, based on neighbs within max_range
        # use LQA to estimate PRR based on distance
        for n in store.get_nodes():
            neighbs = store.find_neighbours(n)
            self.node_degree_distr.append(len(neighbs))
            for nb in neighbs:
                # distance between nodes
                dist = sqrt((n.x - nb.x)*(n.x - nb.x) + (n.y - nb.y)*(n.y-nb.y))
                if dist == 0:
                    #print "Error - distance 0 between", n._id, nb._id, (n.x, n.y), (nb.x, nb.y)
                    continue
                self.add_edge(n._id, nb._id, LQA.get_prr(dist), LQA.get_rssi(dist), 100)

    def get_avg_node_degree(self):
        return sum(self.node_degree_distr)/len(self.node_degree_distr)

    def output(self):
        return "<radiomedium>\n\
                org.contikios.cooja.radiomediums.DirectedGraphMedium\n" +\
                '\n'.join([str(e) for e in self.edges]) +"\n"+\
                "</radiomedium>"
