import cooja_control.grid_net as grid_net
from cooja_control.network_builder import Node, NodeStore
import networkx as nx
import subprocess
from sys import stdout

# TODO: configure contiki path
contiki='/home/victor/kits/contiki'

def run_experiment(base_folder, exp_name, motes, net_size = 100, exp_length = 60, remove_nodes=0, move_nodes=0):
    """Creates a random grid network and runs an experiment in Cooja
        
       Start with a square grid (each node has 4 neighbours).
       The network can be randomised by removing and/or moving some nodes. The algorithm
       will ensure that the network stays connected even after the changes.

        base_folder --- Specify the folder where to save the experiment results
        exp_name    --- Name added to the experiment log files to distinguish them.
        motes       --- Details of the mote types to be deployed in the network.
                        {'type':<string, e.g sky1>, 'path':<path to firmware>,
                         'number':<number of motes of this type>}
        net_size    --- Network size: needs to be equal to the total number of motes
        exp_length  --- Experiment length, in seconds
        remove_nodes -- Will remove that many nodes from the network. Default is 0.
        move_nodes   -- Will move that many nodes from the network. Default is 0.
                        Happens after removing the nodes

    Generates pcap, csv for packet capture.
    PCAP contains all packets. CSV contains only DIO messages.
    Generates log for debug logs.
    """

    #------------------------------------------------------
    # Some constants
    #----------------
    radio_range = 10  # comms distance

    # generate network
    (area_size, net) = grid_net.grid_net(net_size, radio_range)

    # generate node store for getting node neighbours
    store = grid_net.net_store_from_net(net, area_size, radio_range)
    

    
    nodes = store.get_nodes()
    # sort the nodes on their _id
    nodes.sort(key=(lambda x:x._id))
    
    # generate Cooja scripts
    base_path = (base_folder+'/net_%d_range_' + exp_name) % (radio_range)
    grid_net.generate_script_with_net(base_path, int(radio_range)+1, net, exp_length, motes)
    
    # run Cooja simulation
    print "Starting simulation"
    print 'java -mx1024m -jar %s/tools/cooja/dist/cooja.jar -nogui=%s.csc -contiki=%s' % (contiki, base_path, contiki)
    proc = subprocess.Popen('java -mx1024m -jar %s/tools/cooja/dist/cooja.jar -nogui=%s.csc -contiki=%s' % (contiki, base_path, contiki), stdout=subprocess.PIPE, shell=True)
    for line in proc.stdout:
        print line
    print "Done"
    # convert pcap file to csv, filtering on DIO messages
    #!tshark -r {base_path}.pcap -e frame.number -e ipv6.src -e ipv6.dst -e _ws.col.Info -Y "icmpv6.type==155&&icmpv6.code==1" -T fields -E separator=, -E quote=d > {base_path}.csv

