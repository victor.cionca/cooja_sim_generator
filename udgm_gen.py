class UDGMConfig:
    def __init__(self, tx_range, interference_range, tx_success, rx_success):
        self.config = "\
                <radiomedium>\n\
                org.contikios.cooja.radiomediums.UDGM\n\
                <transmitting_range>%d</transmitting_range>\n\
                <interference_range>%d</interference_range>\n\
                <success_ratio_tx>%1.2f</success_ratio_tx>\n\
                <success_ratio_rx>%1.2f</success_ratio_rx>\n\
                </radiomedium>" % (tx_range, interference_range, tx_success, rx_success)

    def output(self):
        return self.config
