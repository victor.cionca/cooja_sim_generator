"""
Determine link quality based on distance using Friis Eq
"""

from math import sqrt, log10, pi, erf
from scipy.special import erfinv

wavelength = 0.125  # based on 2.4GHz radios
ptx = 0             # 0dBm, max tx power for WSN
# no antenna gain
rx_sensitivity = -94# receiver sensitivity, -94dBm (CC2420)

def get_max_range():
    """Determines max range, where RSSI = -94.
    Uses Friis equation.
    """
    return 500 # roughly, based on the above parameters

def get_rssi(distance):
    """Compute RSSI (received power), in dBm, based on distance"""
    rssi = ptx + 20*log10(wavelength/(4*pi*distance))
    if rssi < -110:
        return -110
    else:
        return rssi

def can_reach(distance):
    """Can a radio successfully send packets over distance?
    Depends on the configured receiver sensitivity"""
    return get_rssi(distance) > rx_sensitivity

def get_prr(distance):
    """Estimate the PRR at distance.
    First compute the RSSI with Friis equation.
    Then use a poor model for RSSI-PRR relation.

    This should under no circumstance be used for
    realistic modelling.
    """
    rssi = get_rssi(distance)
    return erf(sqrt(2*(rssi+110)/8.0))**96

def get_min_rssi(prr):
    """Determine the minimum RSSI required to obtain the given PRR"""
    return 4*erfinv(prr**(1./96))**2-110

def get_max_dist_for_rssi(rssi):
    """Determine the maximum distance where the given RSSI is obtained"""
    return (wavelength/(4*pi))*(10**(-(rssi-ptx)/20.))

def get_max_dist_for_prr(prr):
    """Determing the maximum distance where the given PRR is obtained"""
    return get_max_dist_for_rssi(get_min_rssi(prr))
