"""
This is a base class for network layout generators.
The functionality is to build a network consisting
of node positions as (x,y) tuples.
"""

class LayoutGenerator():
    def __init__(self, area_width, area_length, num_nodes):
        self.area_width = area_width
        self.area_height = area_height
        self.num_nodes = num_nodes

    def generate_net(self):
        pass
