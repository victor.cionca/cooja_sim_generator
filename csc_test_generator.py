"""
The function csc_test_generator is used to generate a CSC script.
It takes as input
    * a configured radio model object
    * a list of node descriptors (type NodeDescriptor)
    * the filename
    * the experiment length (in seconds)
    * a configuration dictionary
"""

from test_gen import SimScriptBuilder

class NodeDescriptor:
    def __init__(self, x, y, node_id, node_type, app_path, firmware):
        """Descriptor for CSC file.
        Parameters:
        x,y     -- position
        node_type       -- String label associated to this application (eg 'sky1')
        app_path        -- Path to firmware binary (must end in .sky or .exe)
        firmware        -- At the moment, only 'sky'
        """
        self.x = x
        self.y = y
        self.node_id = node_id
        self.node_type = node_type
        self.app_path = app_path
        self.firmware = firmware

def csc_test_generator(radio_model, nodes, csc_file, exp_length, script_config):
    """Generates a Cooja simulation configuration (CSC) 
    that uses the radio_model and the list of nodes.
    Each node is described with an instance of NodeDescriptor.

    csc_file        -- path and filename of target CSC file
    exp_length      -- int, experiment length in seconds
    script_config   -- dictionary of configuration options.
    The following are accepted:

    script_config keys:
    enable_serial      -- boolean, default False
    base_serial        -- int
    random_seed        -- int value, seed for PRNG
    pcap               -- True if pcap file should be generated, default False
    log                -- True if log file containing printf should be generated,
                          default False
    """
    # determine base path for scripts. Warning - we assume it ends in ".csc"
    base_path = csc_file[:-4]
    
    script_builder = SimScriptBuilder(csc_file, exp_length*1000, radio_model, **script_config)
    if 'random_seed' in script_config:
        script_builder.set_random_seed(script_config['random_seed'])

    # create a dictionary of the mote apps, based on the node list
    # and add the mote apps to the builder
    mote_apps = dict()
    for index, n in enumerate(nodes):
        if n.node_type not in mote_apps:
            mote_apps[n.node_type] = (n.app_path, n.firmware)
            script_builder.add_mote_app(n.node_type, n.app_path, n.firmware)
        script_builder.add_mote(n.x, n.y, n.node_id, n.node_type)

    # enable plugins
    if 'pcap' in script_config:
        script_builder.enable_logging(base_path+'.pcap')
    if 'log' in script_config:
        script_builder.enable_debug(base_path+'.log')

    # generate the script
    script_builder.generate()
