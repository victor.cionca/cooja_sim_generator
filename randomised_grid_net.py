import networkx as nx
from cooja_control.grid_net import GridLayout
import random
from math import sqrt

class RandomisedGridLayout():

    def __init__(self, num_nodes, distance, comms_range, ratio_moved, ratio_removed,
                moving_range=0):
        """Generate a grid with a percentage of nodes moved and 
        another percentage removed to add a bit of randomness to
        a regular grid.
        The final network is still connected, forming a single
        partition.

        Parameters
        num_nodes   --  number of nodes in the original grid
        distance    --  distance between nodes
        comms_range --  communication range (should be > distance)
        ratio_moved    -- how many nodes to move, between 0 and 1
        ratio_removed  -- how many nodes to remove, between 0 and 1
        moving_range    -- nodes move only within moving_range percent of 
                           comms range from original position (in X and Y).
                           If 0 there are no bounds and the node can move 
                           anywhere on the map. Value is in [0,1]
        """
        # underneath there's an initial grid
        self.grid = GridLayout(num_nodes, distance)
        self.comms_range = comms_range
        self.ratio_moved = ratio_moved
        self.ratio_removed = ratio_removed
        self.moving_range = int(moving_range * comms_range)

    def generate_net(self):
        """Generate the coordinates of the network ndoes
        """
        # Generate a node store to keep track of neighbours
        store = self.grid.net_store_from_net(self.comms_range)

        # generate a graph to store network
        net_graph = nx.Graph()
        for node in store.get_nodes():
            net_graph.add_node(node._id, data=node)
            for neighb in store.find_neighbours(node):
                net_graph.add_edge(node._id, neighb._id)

        # remove some nodes at random
        remove_nodes = int(self.grid.num_nodes*self.ratio_removed)
        if remove_nodes > 0:
            random.seed()
            to_delete = []
            nodes = list(range(1,len(self.grid.nodes)))   # don't delete the sink...
            while remove_nodes > 0:
                n = random.sample(nodes, 1)[0]
                # try to delete and check that we still have a connected graph
                n_data = net_graph.node[n]['data']
                net_graph.remove_node(n)
                if nx.is_connected(net_graph):
                    remove_nodes -= 1
                    nodes.remove(n)
                    to_delete.append(n) # schedule to remove from net
                    store.remove_node(n_data) # remove from store
                else:
                    net_graph.add_node(n, {'data':n_data})
                    for neighb in store.find_neighbours(n_data):
                        net_graph.add_edge(n, neighb._id)
            if not nx.is_connected(net_graph):
                raise Exception('Network is not connected!!!')

            # remove from network
            to_delete.sort(reverse = True)
            for _idx in to_delete:
                self.grid.nodes[_idx] = None    # leave it like that for later

        # move some nodes at random
        move_nodes = int(self.grid.num_nodes*self.ratio_moved)
        if move_nodes > 0:
            random.seed()
            to_move = random.sample(list(net_graph)[1:], move_nodes)
            for n in to_move:
                # move the node in the store (remove, respawn, add) until it has neighbours
                n_data = net_graph.nodes[n]['data']
                store.remove_node(n_data)
                orig_x = n_data.x
                orig_y = n_data.y
                while True:
                    if self.moving_range:
                        _x = random.randint(max(n_data.x-self.moving_range,0),
                            min(n_data.x+self.moving_range, self.grid.net_area_side))
                        _y = random.randint(max(n_data.y-self.moving_range,0),
                            min(n_data.y+self.moving_range, self.grid.net_area_side))
                    else:
                        _x = random.randint(0, self.grid.net_area_side)
                        _y = random.randint(0, self.grid.net_area_side)
                    #print('Moving %d'%sqrt((n_data.x-_x)**2 + (n_data.y-_y)**2))
                    n_data.move(_x, _y)
                    neighbs = store.find_neighbours(n_data)
                    if len(neighbs) > 0:
                        #n_data.neighbs = neighbs
                        # This is not enough, we need to check for graph connectedness
                        # because the move might leave a disconnected partition
                        net_graph.remove_node(n)
                        net_graph.add_node(n, data=n_data)
                        for nb in neighbs:
                            net_graph.add_edge(n, nb._id)
                        if nx.is_connected(net_graph):
                            break
                        else:
                            # Restore n_data to its initial position
                            n_data.move(orig_x, orig_y)

                store.add(n_data)
                self.grid.nodes[n] = (n_data.x, n_data.y)

        # remove any None values in the network
        while True:
            try:
                self.grid.nodes.remove(None)
            except ValueError:
                break

        return self.grid.nodes, store
