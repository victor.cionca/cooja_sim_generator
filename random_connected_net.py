"""
This creates a network where the nodes are randomly
placed on the area while still maintaining network
connectivity.
"""

from random import randint
from math import sqrt
from cooja_control.network_builder import Node, NodeStore
import cooja_control.net_link_quality_analyser as LQA

class RandomConnectedLayout():
    def __init__(self, area_width, num_nodes):
        self.area_width = area_width
        self.num_nodes = num_nodes
        self.nodes = []
        self.store = None

    def generate_net(self):
        """
        Creates a network of @area_width, with @num_nodes 
        communicating over @radio_range

        Returns the list of nodes
        """
        max_range = LQA.get_max_range()
        net_diag = int(sqrt(2)*self.area_width)
        self.store = NodeStore(net_diag, max_range)
        # add the first node separately
        n = Node(randint(0,self.area_width), randint(0,self.area_width))
        self.store.add(n)
        self.nodes.append((n.x, n.y))
        for i in range(self.num_nodes-1):
            neighbs = []
            # to keep the network connected, every newly added node
            # must have at least one neighbour
            while len(neighbs) == 0:
                n = Node(randint(0,self.area_width), randint(0,self.area_width), _id=i+1)
                neighbs = self.store.find_neighbours(n)
            for nb in neighbs:
                nb.neighbs.append(n)
                n.neighbs.append(nb)
            self.store.add(n)
            self.nodes.append((n.x, n.y))
        return self.nodes

if __name__ == '__main__':
    rand_layout = RandomConnectedLayout(50, 50, 50)
    print(rand_layout.generate_net())
